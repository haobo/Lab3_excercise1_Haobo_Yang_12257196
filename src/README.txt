Here is the instruction for how to play with the game.

Run the program in PlayCluedo. In the start interface, choose the number of player (3-6).Then you enter the game interface. The interface is divided into 5 area. 

The top area is used to display the instruction for players. When the mouse moves to the game board, it will list the position of the slot. When the mouse moves to button area (right side of the interface), it will ask the player to choose operation. 

The left hand side is the recording area to record all the process in the game for all of the players. Such as player turn, dice through number, movement and card ownership.

The bottom area is used to display the position of each pawn.

The middle area is the game board. The room position and pawns are displayed in the game board. 

The right area is for operation buttons.

To start the game, player throw the dices first. Every time the player through the dices, the slots player can go will are highlight in the game board. The original slot and a slot taken by other pawn are not available. Then he can raise a hypothesis. The hypothesis must be a card from suspect and a card of weapon. The room must be the room the player already in. After that, click polling, if the player is in a room, the system will automatically display an owner of one of the three cards. Otherwise, the pawn is at corridor, the top area will told the player that he cannot know the card ownership. If the player want, he can raise an accusation. And the interface will show the result whether he is win or not.

All the information will are recorded in notebook. Player can access the notebook whenever they like. The cards the player own will show 1 for the whole row. The player which doesn��t own he card will show 0. And when the player no other player own one card, the notebook will fill 1 for this player for the specific card and fill 0 for other players.And 7 stands for "?" which means doesn't know the owenersheip.

Every time after the player polling and then open the notebook, once he close the notebook, it will goes to next player's turn. (The way to notice the system to go to next one's turn)
