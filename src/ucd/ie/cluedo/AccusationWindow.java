package ucd.ie.cluedo;
//class for showing a new window for let player do a  Hypothesis( using JRadioButton buttons) and it is the final window.( mostly same as HypothesisWindow and
// withour the difficult of collect information from child class)
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JRadioButton;

import ucd.ie.cluedo.CardType.RoomName;
import ucd.ie.cluedo.CardType.SuspectName;
import ucd.ie.cluedo.CardType.WeaponName;
public class AccusationWindow extends JFrame implements ActionListener{
	private JRadioButton Colonel_Mustard, Professor_Plum, Rev_Green, Mrs_Peacock, Miss_Scarlet, Mrs_White, Kitchen, Ballroom, Conservatory, Dining_Room, Library, Hall, Study, Billiard_Room, Lounge, 
						 Candlestick, Knife, lead_Pipe, Revolver, Rope, Poison;
	private ButtonGroup Suspect, Room, Weapon;
	private JButton confirm;
	private Card[] result;
	private Card[] truth;
	private int player;
	public AccusationWindow(Card[] truth, int player){
		this.setTitle("Accusation!");
		this.player = player;
		this.truth = truth;
		result = new Card[3];
		this.setSize(800, 900);
    	this.setLocation( this.getToolkit().getScreenSize().width / 2- this.getWidth() / 2, 
                this.getToolkit().getScreenSize().height/ 2 - this.getHeight() / 2);

		this.getContentPane().setLayout(new GridLayout(4,1));
		Panel p1 = new Panel();
		p1.setLayout(new GridLayout(2,4));
		Panel p2 = new Panel();
		p2.setLayout(new GridLayout(2,5));
		Panel p3 = new Panel();
		p3.setLayout(new GridLayout(2,4));
    	Font font1 = new Font("Courier", Font.BOLD,24);
		Suspect = new ButtonGroup();
		Colonel_Mustard = new JRadioButton("Colonel_Mustard");
		Colonel_Mustard.setBackground(Color.black);
		Colonel_Mustard.setForeground(Color.gray);
		Professor_Plum = new JRadioButton("Professor_Plum");
		Professor_Plum.setForeground(Color.red);
		Rev_Green = new JRadioButton("Rev_Green");
		Rev_Green.setBackground(Color.black);
		Rev_Green.setForeground(Color.gray);
		Mrs_Peacock = new JRadioButton("Mrs_Peacock");
		Mrs_Peacock.setBackground(Color.black);
		Mrs_Peacock.setForeground(Color.gray);
		Miss_Scarlet = new JRadioButton("Miss_Scarlet");
		Miss_Scarlet.setForeground(Color.red);
		Mrs_White = new JRadioButton("Mrs_White");
		Mrs_White.setBackground(Color.black);
		Mrs_White.setForeground(Color.gray);
		Suspect.add(Colonel_Mustard);
		Suspect.add(Professor_Plum);
		Suspect.add(Rev_Green);
		Suspect.add(Mrs_Peacock);
		Suspect.add(Miss_Scarlet);
		Suspect.add(Mrs_White);
		Room = new ButtonGroup();
		Kitchen = new JRadioButton("Kitchen");
		Kitchen.setForeground(Color.red);
		Ballroom = new JRadioButton("Ballroom");
		Ballroom.setBackground(Color.black);
		Ballroom.setForeground(Color.gray);
		Conservatory = new JRadioButton("Conservatory");
		Conservatory.setForeground(Color.red);
		Dining_Room = new JRadioButton("Dining_Room");
		Dining_Room.setBackground(Color.black);
		Dining_Room.setForeground(Color.gray);
		Library = new JRadioButton("Library");
		Library.setForeground(Color.red);
		Hall = new JRadioButton("Hall");
		Hall.setBackground(Color.black);
		Hall.setForeground(Color.gray);
		Study = new JRadioButton("Study");
		Study.setForeground(Color.red);
		Billiard_Room = new JRadioButton("Billiard_Room");
		Billiard_Room.setBackground(Color.black);
		Billiard_Room.setForeground(Color.gray);
		Lounge = new JRadioButton("Lounge");
		Lounge.setForeground(Color.red);
		Room.add(Kitchen);
		Room.add(Ballroom);
		Room.add(Conservatory);
		Room.add(Dining_Room);
		Room.add(Library);
		Room.add(Hall);
		Room.add(Study);
		Room.add(Billiard_Room);
		Room.add(Lounge);
		Weapon = new ButtonGroup();
		Candlestick = new JRadioButton("Candlestick");
		Candlestick.setBackground(Color.black);
		Candlestick.setForeground(Color.gray);
		Knife = new JRadioButton("Knife");
		Knife.setForeground(Color.red);
		lead_Pipe = new JRadioButton("lead_Pipe");
		lead_Pipe.setBackground(Color.black);
		lead_Pipe.setForeground(Color.gray);
		Revolver = new JRadioButton("Revolver");
		Revolver.setBackground(Color.black);
		Revolver.setForeground(Color.gray);
		Rope = new JRadioButton("Rope");
		Rope.setForeground(Color.red);
		Poison = new JRadioButton("Poison");
		Poison.setBackground(Color.black);
		Poison.setForeground(Color.gray);

		Weapon.add(Candlestick);
		Weapon.add(Knife);
		Weapon.add( lead_Pipe);
		Weapon.add( Revolver);
		Weapon.add(Rope);
		Weapon.add(Poison);
		p1.add(new JLabel("Suspect:"));
		p1.add(Colonel_Mustard);
		p1.add(Professor_Plum);
		p1.add(Rev_Green);
		p1.add(Mrs_Peacock);
		p1.add(Miss_Scarlet);
		p1.add(Mrs_White);
		p2.add(new JLabel("Room:"));
		p2.add(Kitchen);
		p2.add(Ballroom);
		p2.add(Conservatory);
		p2.add(Dining_Room);
		p2.add(Library);
		p2.add(Hall);
		p2.add(Study);
		p2.add(Billiard_Room);
		p2.add(Lounge);
		p3.add(new JLabel("Weapon:"));
		p3.add(Candlestick);
		p3.add(Knife);
		p3.add(lead_Pipe);
		p3.add(Revolver);
		p3.add(Rope);
		p3.add(Poison);
    	p3.setFont(font1);
		confirm = new JButton("confirm");
		confirm.setBackground(Color.red);
		confirm.setForeground(Color.black);
		this.getContentPane().add(p1);
		this.getContentPane().add(p2);
		this.getContentPane().add(p3);
		this.getContentPane().add(confirm);
		confirm.addActionListener(this);
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		this.setVisible(true);
	}
		public void actionPerformed(ActionEvent e) {
			if(e.getSource() == confirm){
				if(Colonel_Mustard.isSelected()){
					result[0] = new SuspectCard(SuspectName.Colonel_Mustard);
				}
				if(Professor_Plum.isSelected()){
					result[0] = new SuspectCard(SuspectName.Professor_Plum);
				}
				if(Rev_Green.isSelected()){
					result[0] = new SuspectCard(SuspectName.Rev_Green);
				}
				if(Mrs_Peacock.isSelected()){
					result[0] = new SuspectCard(SuspectName.Mrs_Peacock);
				}
				if(Miss_Scarlet.isSelected()){
					result[0] = new SuspectCard(SuspectName.Miss_Scarlet);
				}
				if(Mrs_White.isSelected()){
					result[0] = new SuspectCard(SuspectName.Mrs_White);
				}
				if(Kitchen.isSelected()){
					result[1] = new RoomCard(RoomName.Kitchen);
				}
				if(Ballroom.isSelected()){
					result[1] = new RoomCard(RoomName.Ballroom);
				}
				if(Conservatory.isSelected()){
					result[1] = new RoomCard(RoomName.Conservatory);
				}
				if(Dining_Room.isSelected()){
					result[1] = new RoomCard(RoomName.Dining_Room);
				}
				if(Library.isSelected()){
					result[1] = new RoomCard(RoomName.Library);
				}
				if(Hall.isSelected()){
					result[1] = new RoomCard(RoomName.Hall);
				}
				if(Study.isSelected()){
					result[1] = new RoomCard(RoomName.Study);
				}
				if(Billiard_Room.isSelected()){
					result[1] = new RoomCard(RoomName.Billiard_Room);
				}
				if(Lounge.isSelected()){
					result[1] = new RoomCard(RoomName.Lounge);
				}
				if(Candlestick.isSelected()){
					result[2] = new WeaponCard(WeaponName.Candlestick);
				}
				if(Knife.isSelected()){
					result[2] = new WeaponCard(WeaponName.Knife);
				}
				if(lead_Pipe.isSelected()){
					result[2] = new WeaponCard(WeaponName.lead_Pipe);
				}
				if(Revolver.isSelected()){
					result[2] = new WeaponCard(WeaponName.Revolver);
				}
				if(Rope.isSelected()){
					result[2] = new WeaponCard(WeaponName.Rope);
				}
				if(Poison.isSelected()){
					result[2] = new WeaponCard(WeaponName.Poison);
				}
				this.setVisible(false);
				if(truth[0].GetInfo().equals(result[0].GetInfo()) && truth[1].GetInfo().equals(result[1].GetInfo()) && truth[2].GetInfo().equals(result[2].GetInfo())){
					Win win = new Win(true, player);
				}else{
					Win win = new Win(false, player);
				}
			}
		}
		
	}