package ucd.ie.cluedo;

import ucd.ie.cluedo.CardType.CardsType;
// original object class for cards attribute

public interface Card {
	String GetInfo();
	CardsType GetType();
	void SetOwner(Player owner);
	Player GetOwner();
}
