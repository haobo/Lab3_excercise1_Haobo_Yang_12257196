package ucd.ie.cluedo;

	public final class CardType {
		public enum CardsType {
			Suspect, Room, Weapon;
		}
		public enum SuspectName {
			Colonel_Mustard, Professor_Plum, Rev_Green, Mrs_Peacock, Miss_Scarlet, Mrs_White;
		}
		public enum RoomName {
			Corridor, Kitchen, Ballroom, Conservatory, Dining_Room, Library, Hall, Study, Billiard_Room, Lounge;
		}
		public enum WeaponName {
			Candlestick, Knife, lead_Pipe, Revolver, Rope, Poison;
		}
	}

