package ucd.ie.cluedo;

import java.util.ArrayList;
import java.util.Arrays;

import ucd.ie.cluedo.CardType.RoomName;
import ucd.ie.cluedo.CardType.SuspectName;
import ucd.ie.cluedo.CardType.WeaponName;
// this class distribute cards to new players, and pick three cards for truth.
public class CardsandPlayersAction {
	private Player[] players;
	private Card[] dc;
	private SuspectCard suspect;
	private RoomCard room;
	private WeaponCard weapon;
	private int random;
	private int cc;
	private int pc;	
	public CardsandPlayersAction(int playercount){	
		players = new Player[playercount];
		Card[] nb = {new SuspectCard(SuspectName.Colonel_Mustard), new SuspectCard(SuspectName.Professor_Plum), new SuspectCard(SuspectName.Rev_Green),
				   new SuspectCard(SuspectName.Mrs_Peacock), new SuspectCard(SuspectName.Miss_Scarlet),new SuspectCard(SuspectName.Mrs_White),
				   new RoomCard(RoomName.Kitchen),  new RoomCard(RoomName.Ballroom),  new RoomCard(RoomName.Conservatory),
				   new RoomCard(RoomName.Dining_Room),  new RoomCard(RoomName.Library),  new RoomCard(RoomName.Hall),  new RoomCard(RoomName.Study),
				   new RoomCard(RoomName.Billiard_Room),  new RoomCard(RoomName.Lounge), new WeaponCard(WeaponName.Candlestick), new WeaponCard(WeaponName.Knife), 
				   new WeaponCard(WeaponName.lead_Pipe) , new WeaponCard(WeaponName.Revolver) , new WeaponCard(WeaponName.Rope) ,
				   new WeaponCard(WeaponName.Poison) };
		ArrayList<Card> al =  new ArrayList<Card>(Arrays.asList(nb));
		random = (int)(100*Math.random()%playercount);		
		suspect = (SuspectCard)nb[random];
		al.remove(suspect);
		random = (int)(100*Math.random()%9) + 6;
		room = (RoomCard)nb[random];
		al.remove(room);
		random = (int)(100*Math.random()%6) + 15;
		weapon = (WeaponCard)nb[random];
		al.remove(weapon);
		dc = (Card[]) al.toArray(new Card[al.size()]);
		for(int i = 0;i<dc.length;i++){
			Card temp = dc[i];
			random = (int)(100*Math.random()%dc.length);
			dc[i] = dc[random];
			dc[random] = temp;
		}
		for(int j = 0; j < playercount;j++){
			players[j]= new Player(playercount);
		}
		cc = 0;
		pc = 0;
		while(dc.length != cc){
				players[pc].PickCard(dc[cc]);
				pc++;
				cc++;	
				if(pc == playercount){
					pc = 0;
				}
		}
	}
	public Player[] PlayerInfo(){
		Card[] result =  new Card[3];
		result[0] = suspect;
		result[1] = room;
		result[2] = weapon;
		return players ;	
	}
	public Card[] Result(){
		Card[] result =  new Card[3];
		result[0] = suspect;
		result[1] = room;
		result[2] = weapon;
		return result;	
	}
}
