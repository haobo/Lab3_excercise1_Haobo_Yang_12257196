package ucd.ie.cluedo;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTextArea;
// class for joint CardsandPlayersAction and PawnandGameBoardAction 
public class Cluedo extends JFrame implements ActionListener{
	private int playercount;
	private JMenuItem three, four, five, six;
	public Cluedo(){
		this.setTitle("Cluedo");
		this.setSize(500, 400);
    	this.setLocation( this.getToolkit().getScreenSize().width / 2- this.getWidth() / 2, 
                this.getToolkit().getScreenSize().height/ 2 - this.getHeight() / 2);
    	this.setLayout(new BorderLayout(20,20));
    	Font font = new Font("Courier", Font.BOLD,24);
    	JTextArea p1 = new JTextArea("");
    	p1.setPreferredSize(new Dimension(200,100));
    	p1.setBackground(Color.black);
    	p1.setForeground(Color.gray);
    	p1.setFont(font);
    	p1.setCaretColor(Color.black);
    	p1.setEditable(false);
    	p1.setText(" \n\n\n\n                 welcome to cluedo!\n           Please choose player number");
        JPanel p=new JPanel();  
        JPopupMenu menu=new JPopupMenu();  
        three = new JMenuItem("Three");
        four = new JMenuItem("four");
        five = new JMenuItem("five");
        six = new JMenuItem("six");
        menu.add(three);  
        menu.add(four);  
        menu.add(five);  
        menu.add(six);  
        three.addActionListener(this); 
        four.addActionListener(this); 
        five.addActionListener(this); 
        six.addActionListener(this); 
        MenuButton button = new MenuButton("Player Number");  
        button.addMenu(menu);   
        p.add(button);  
        this.add(p,BorderLayout.SOUTH);  
        this.add(p1,BorderLayout.CENTER);  
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);  
        this.setVisible(true);  
        
       
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == three){
			playercount = 3;	
			this.Start();
			this.setVisible(false);  
		}
		if(e.getSource() == four){
			playercount = 4;	
			this.Start();
			this.setVisible(false);  
		}
		if(e.getSource() == five){
			playercount = 5;
			this.Start();
			this.setVisible(false);  
		}
		if(e.getSource() == six){
			playercount = 6;	
			this.Start();
			this.setVisible(false);  
		}
	}
	public void Start() {
		CardsandPlayersAction Ca = new CardsandPlayersAction(playercount);
		PawnandGameBoardAction Gb = new PawnandGameBoardAction(playercount, Ca.PlayerInfo(), Ca.Result());

	}
	
}
