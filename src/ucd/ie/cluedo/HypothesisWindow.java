package ucd.ie.cluedo;
// class for showing a new window for let player do a  Hypothesis( using JRadioButton buttons)
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JRadioButton;

import ucd.ie.cluedo.CardType.RoomName;
import ucd.ie.cluedo.CardType.SuspectName;
import ucd.ie.cluedo.CardType.WeaponName;
public class HypothesisWindow extends JFrame implements ActionListener{
	private JRadioButton Colonel_Mustard, Professor_Plum, Rev_Green, Mrs_Peacock, Miss_Scarlet, Mrs_White, room, 
						 Candlestick, Knife, lead_Pipe, Revolver, Rope, Poison;
	private ButtonGroup Suspect, Room, Weapon;
	private JButton confirm;
	private Card[] result;
	private RoomName rm;
	private int success;
	// in constructor I add my seclections for play and it also make sure people can rise a Hypothesis just when they in a room.
	public HypothesisWindow(RoomName rm){
		this.setTitle("Hypothesis");
		success = 0;
		this.rm = rm;
		result = new Card[3];
		this.setSize(800, 900);
    	this.setLocation( this.getToolkit().getScreenSize().width / 2- this.getWidth() / 2, 
                this.getToolkit().getScreenSize().height/ 2 - this.getHeight() / 2);

		this.getContentPane().setLayout(new GridLayout(4,1));
		Panel p1 = new Panel();
		p1.setLayout(new GridLayout(2,4));
		Panel p2 = new Panel();
		p2.setLayout(new GridLayout(2,5));
		Panel p3 = new Panel();
		p3.setLayout(new GridLayout(2,4));
    	Font font1 = new Font("Courier", Font.BOLD,24);
		Suspect = new ButtonGroup();
		Colonel_Mustard = new JRadioButton("Colonel_Mustard");
		Professor_Plum = new JRadioButton("Professor_Plum");
		Rev_Green = new JRadioButton("Rev_Green");
		Mrs_Peacock = new JRadioButton("Mrs_Peacock");
		Miss_Scarlet = new JRadioButton("Miss_Scarlet");
		Mrs_White = new JRadioButton("Mrs_White");

		Suspect.add(Colonel_Mustard);
		Suspect.add(Professor_Plum);
		Suspect.add(Rev_Green);
		Suspect.add(Mrs_Peacock);
		Suspect.add(Miss_Scarlet);
		Suspect.add(Mrs_White);
		Room = new ButtonGroup();
		room = new JRadioButton(rm.name());
		Room.add(room);
		Weapon = new ButtonGroup();
		Candlestick = new JRadioButton("Candlestick");
		Knife = new JRadioButton("Knife");
		lead_Pipe = new JRadioButton("lead_Pipe");
		Revolver = new JRadioButton("Revolver");
		Rope = new JRadioButton("Rope");
		Poison = new JRadioButton("Poison");
		Weapon.add(Candlestick);
		Weapon.add(Knife);
		Weapon.add( lead_Pipe);
		Weapon.add( Revolver);
		Weapon.add(Rope);
		Weapon.add(Poison);
		p1.add(new JLabel("Suspect:"));
		p1.add(Colonel_Mustard);
		p1.add(Professor_Plum);
		p1.add(Rev_Green);
		p1.add(Mrs_Peacock);
		p1.add(Miss_Scarlet);
		p1.add(Mrs_White);
		p2.add(new JLabel("Room:"));
		p2.add(room);
		p3.add(new JLabel("Weapon:"));
		p3.add(Candlestick);
		p3.add(Knife);
		p3.add(lead_Pipe);
		p3.add(Revolver);
		p3.add(Rope);
		p3.add(Poison);
    	p3.setFont(font1);
		confirm = new JButton("confirm");

		this.getContentPane().add(p1);
		this.getContentPane().add(p2);
		this.getContentPane().add(p3);
		this.getContentPane().add(confirm);
		confirm.addActionListener(this);
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		this.setVisible(true);
	}
		//setting how to use my result of the three MCQ
		public void actionPerformed(ActionEvent e) {
			if(e.getSource() == confirm){
				if(Colonel_Mustard.isSelected()){
					result[0] = new SuspectCard(SuspectName.Colonel_Mustard);
					success++;
				}
				if(Professor_Plum.isSelected()){
					result[0] = new SuspectCard(SuspectName.Professor_Plum);
					success++;
				}
				if(Rev_Green.isSelected()){
					result[0] = new SuspectCard(SuspectName.Rev_Green);
					success++;
				}
				if(Mrs_Peacock.isSelected()){
					result[0] = new SuspectCard(SuspectName.Mrs_Peacock);
					success++;
				}
				if(Miss_Scarlet.isSelected()){
					result[0] = new SuspectCard(SuspectName.Miss_Scarlet);
					success++;
				}
				if(Mrs_White.isSelected()){
					result[0] = new SuspectCard(SuspectName.Mrs_White);
					success++;
				}
				if(room.isSelected()){
					result[1] = new RoomCard(rm);
					success++;
				}
				if(Candlestick.isSelected()){
					result[2] = new WeaponCard(WeaponName.Candlestick);
					success++;
				}
				if(Knife.isSelected()){
					result[2] = new WeaponCard(WeaponName.Knife);
					success++;
				}
				if(lead_Pipe.isSelected()){
					result[2] = new WeaponCard(WeaponName.lead_Pipe);
					success++;
				}
				if(Revolver.isSelected()){
					result[2] = new WeaponCard(WeaponName.Revolver);
					success++;
				}
				if(Rope.isSelected()){
					result[2] = new WeaponCard(WeaponName.Rope);
					success++;
				}
				if(Poison.isSelected()){
					result[2] = new WeaponCard(WeaponName.Poison);
					success++;
				}
				this.setVisible(false);
			}
		}
		public Card[] GuessResult(){
			return result;
		}
		// this flag for collection information from this class after the result is finished.
		public int Success(){
			return success;
		}
		
	}