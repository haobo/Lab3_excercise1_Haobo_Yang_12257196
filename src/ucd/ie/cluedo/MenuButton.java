package ucd.ie.cluedo;
import java.awt.event.ActionEvent;  
import java.awt.event.ActionListener;  
import javax.swing.JPopupMenu;  
import javax.swing.JToggleButton;  
import javax.swing.SwingConstants;  
// for using togglebutton easily
public class MenuButton extends JToggleButton{  
	private static final long serialVersionUID = 1L;
	private JPopupMenu menu;   
    public MenuButton(final String label){  
        super(label);  
        this.setText(label);  
        this.setHorizontalTextPosition(SwingConstants.RIGHT );  
        addActionListener(new ActionListener(){  
            @Override  
            public void actionPerformed(ActionEvent arg0) {  
                if(isSelected()){  
                    setText(label);  
                    menu.show(MenuButton.this, 0, MenuButton.this.getHeight());  
                }else{  
                    setText(label);  
                    menu.setVisible(false);  
                }  
            }  
        });  
    }  
    public void addMenu(JPopupMenu menu){  
        this.menu=menu;  
    }  
}  
