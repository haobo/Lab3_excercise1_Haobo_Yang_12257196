package ucd.ie.cluedo;

import ucd.ie.cluedo.CardType.RoomName;
import ucd.ie.cluedo.CardType.SuspectName;
import ucd.ie.cluedo.CardType.WeaponName;
// in note book I use double array to record informations
public class NoteBook {
	private Card[] nb = {new SuspectCard(SuspectName.Colonel_Mustard), new SuspectCard(SuspectName.Professor_Plum), new SuspectCard(SuspectName.Rev_Green),
				   new SuspectCard(SuspectName.Mrs_Peacock), new SuspectCard(SuspectName.Miss_Scarlet),new SuspectCard(SuspectName.Mrs_White),
				   new RoomCard(RoomName.Kitchen),  new RoomCard(RoomName.Ballroom),  new RoomCard(RoomName.Conservatory),
				   new RoomCard(RoomName.Dining_Room),  new RoomCard(RoomName.Library),  new RoomCard(RoomName.Hall),  new RoomCard(RoomName.Study),
				   new RoomCard(RoomName.Billiard_Room),  new RoomCard(RoomName.Lounge), new WeaponCard(WeaponName.Candlestick), new WeaponCard(WeaponName.Knife), 
				   new WeaponCard(WeaponName.lead_Pipe) , new WeaponCard(WeaponName.Revolver) , new WeaponCard(WeaponName.Rope) ,
				   new WeaponCard(WeaponName.Poison) };
	private int[][] note;
	private int playercount;
	public NoteBook(int playercount){
		this.playercount = playercount;
		note = new int[21][playercount];
		for(int i = 0;i<21;i++){	
			for(int j = 0;j<playercount;j++){
				note[i][j]=7;
			}
		}
	}
	// this method cooperate with draw cards in play method for note which card you retain.
	public void owned(Card card){
		for(int i = 0;i<nb.length;i++){
			if(card.GetInfo().equals(nb[i].GetInfo())){
				for(int j = 0;j<playercount;j++ ){
				note[i][j]=1;
				}
			}
		}
	}
	// when some one shown a card to you
	public void add(Card card, int playernumber){
		for(int i = 0;i<nb.length;i++){
			if(card.GetInfo().equals(nb[i].GetInfo())){
				for(int m = 0; m<playercount;m++){
					note[i][m]=0;
				}
				note[i][playernumber]=1;
			}
		}
	}
	//When a play declare they d not have which three card 
	public void minus(Card card, int playernumber){
		for(int i = 0;i<nb.length;i++){
			if(card.GetInfo().equals(nb[i].GetInfo())){
				if(note[i][playernumber]!=1)
				note[i][playernumber]=0;
			}
		}
	}
	public int[][] Show(){
		return note;
	}
}
