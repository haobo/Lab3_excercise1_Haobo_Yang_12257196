package ucd.ie.cluedo;

import java.awt.Color;

import ucd.ie.cluedo.CardType.SuspectName;
// class for Pawn's attribute
public class Pawn {
	private Slot Position;
	private SuspectName suspect;
	private Color color;
	public Pawn(SuspectName suspect,Color color,Slot Position){
		this.suspect = suspect;
		this.Position = Position;
		this.color = color;
	}
	public void Move(Slot Position){
		this.Position = Position;
	}
	public Slot GetSlot(){
		return Position;
	}
	public SuspectName GetName(){
		return suspect;
	}
	public Color GetColor(){
		return color;
	}
}
