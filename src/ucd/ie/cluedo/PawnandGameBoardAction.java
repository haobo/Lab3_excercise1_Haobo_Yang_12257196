package ucd.ie.cluedo;


import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import ucd.ie.cluedo.CardType.RoomName;
import ucd.ie.cluedo.CardType.SuspectName;
// this class contains the basic logic of my game and all graphic operation, olso the classes for create the Play board 
public class PawnandGameBoardAction extends JFrame implements MouseMotionListener,MouseListener, ActionListener{
	private JPanel map;
	private JPanel pane2;
    private MainGB MGB;	
	private JTextField lblCurEvent;
	private JTextField positioninfor;	
	private JTextArea textarea;
    private JButton dice;
    private JButton hypothesis;
    private JButton hypothesis_Polling;
    private JButton accusation;
    private JButton notbook;
    private HypothesisWindow hw;
	private Slot[][] gb;
	private Pawn[] pawn;   
	private Player[] players;
	private Card[] truth;
    private ArrayList<Slot> possible;
    private String strEvent;   
    private String inroominfo;
    private int diceresult1;
    private int diceresult2;   
    private int diceEnable;
    private int Choosing;
	private int moveflag;
	private int showmoveable;
	private int playercount; 
	private int playernumber;
	private int rise;
	private int first;
	private int hp;
	private int hp2;
	//in construction I drawn the graphic and physical infomation of my Play Board.
	public PawnandGameBoardAction(int playercount, Player[] players, Card[] truth){
		this.players = players;
		this.truth = truth;
		this.playercount = playercount;
		gb = new Slot[24][24];
		pawn = new Pawn[playercount];
		for(int x = 0; x < 24; x++){
			for(int y = 0;y < 24; y++){
				gb[x][y] = new Slot(x,y);
				if(y == 0 || (y == 5 && x > 18) || (y == 6 && x < 4) || (y == 6 && x == 5) || (y == 7 && x == 8) || (y == 7 && x > 9 && x < 14) || (y == 7 && x == 15) || (y == 7 && x > 17 ) || (y == 9 && x < 8) || (y == 12 && x > 17) || (y == 13 && x > 16 && x < 20) || (y == 13 && x > 20) || (y == 15 && x < 6) || (y == 15 && x == 7) || (y == 18 && x > 16) || (y == 17 && x > 8 && x < 11) || (y == 17 && x > 12 && x < 15) || (y == 18 && x < 6) || (y == 20 && x > 17)){
					gb[x][y].SetUpIsWall();
				}
				if(x == 0 || ( y < 6 && x == 6 ) || ( y > 17 && x == 7 ) || ( y < 5 && x == 8 ) || (y == 6 && x == 8 ) ||(y == 9 && x == 8 ) ||(y > 10 && y < 15 && x == 8 ) || (y > 16 && x == 9 ) || (y < 19 && y > 16 && x == 15 ) || (y > 19 && x == 15 ) || (y < 5 && x == 16 ) || (y ==6 && x == 16 ) || (y > 12 && y < 15 && x == 17 ) || (y > 15 && y < 18 && x == 17 ) || (y > 19 && x == 17 ) || (y < 5 && x == 18 ) || (y > 6 && y < 8 && x == 18 ) || (y > 8 && y < 12 && x == 18 )){
					gb[x][y].SetLeftIsWall();
				}
				if(y == 23 || (y == 4 && x > 18) || (y == 5 && x < 4) || (y == 5 && x == 5) || (y == 6 && x == 8) || (y == 6 && x > 9 && x < 14) || (y == 6 && x == 15) || (y == 6 && x > 17 ) || (y == 8 && x < 8) || (y == 11 && x > 17) || (y == 12 && x > 16 && x < 20) || (y == 12 && x > 20) || (y == 14 && x < 6) || (y == 14 && x == 7) || (y == 17 && x > 16) || (y == 16 && x > 8 && x < 11) || (y == 16 && x > 12 && x < 15) || (y == 17 && x < 6) || (y == 19 && x > 17)){
					gb[x][y].SetDownIsWall();
				}
				if(x == 23 || ( y < 6 && x == 5 ) || ( y > 17 && x == 6 ) || ( y < 5 && x == 7 ) || (y == 6 && x == 7 ) ||(y == 9 && x == 7 ) ||(y > 10 && y < 15 && x == 7 ) || (y > 16 && x == 8 ) || (y < 19 && y > 16 && x == 14 ) || (y > 19 && x == 14 ) || (y < 5 && x == 15 ) || (y ==6 && x == 15 ) || (y > 12 && y < 15 && x == 16 ) || (y > 15 && y < 18 && x == 16 ) || (y > 19 && x == 16 ) || (y < 5 && x == 17 ) || (y > 6 && y < 8 && x == 17 ) || (y > 8 && y < 12 && x == 17 )){
					gb[x][y].SetRightIsWall();
				}
				if(y < 6 && x < 6){
					gb[x][y].SetInRoom(RoomName.Kitchen);
				}
				if(y < 7 && x > 7 && x < 16){
					gb[x][y].SetInRoom(RoomName.Ballroom);
				}
				if(y < 5 && x > 17 ){
					gb[x][y].SetInRoom(RoomName.Conservatory);
				}
				if(y > 8 && y < 15 && x < 8){
					gb[x][y].SetInRoom(RoomName.Dining_Room);
				}
				if(y > 17 && x < 7){
					gb[x][y].SetInRoom(RoomName.Lounge);
				}
				if(y > 16 && x > 8  && x < 15){
					gb[x][y].SetInRoom(RoomName.Hall);
				}
				if(y > 19 && x > 16){
					gb[x][y].SetInRoom(RoomName.Study);
				}
				if(y > 6 && y < 12 && x > 17){
					gb[x][y].SetInRoom(RoomName.Billiard_Room);
				}
				if(y > 12 && y < 18 && x > 16){
					gb[x][y].SetInRoom(RoomName.Library);
				}
			}
		}
		if(playercount == 3){
			pawn[0] = new Pawn(SuspectName.Colonel_Mustard,Color.yellow, gb[11][11]);
			pawn[1] = new Pawn(SuspectName.Professor_Plum,Color.pink , gb[12][11]);
			pawn[2] = new Pawn(SuspectName.Rev_Green,Color.green , gb[11][12]);
			}
		if(playercount == 4){
			pawn[0] = new Pawn(SuspectName.Colonel_Mustard,Color.yellow, gb[11][11]);
			pawn[1] = new Pawn(SuspectName.Professor_Plum,Color.pink , gb[12][11]);
			pawn[2] = new Pawn(SuspectName.Rev_Green,Color.green , gb[11][12]);
			pawn[3] = new Pawn(SuspectName.Mrs_Peacock,Color.blue , gb[12][12]);
			}
		if(playercount == 5){
			pawn[0] = new Pawn(SuspectName.Colonel_Mustard,Color.yellow, gb[11][10]);
			pawn[1] = new Pawn(SuspectName.Professor_Plum,Color.pink , gb[12][10]);
			pawn[2] = new Pawn(SuspectName.Rev_Green,Color.green , gb[11][11]);
			pawn[3] = new Pawn(SuspectName.Mrs_Peacock,Color.blue , gb[12][12]);
			pawn[4] = new Pawn(SuspectName.Miss_Scarlet,Color.red , gb[11][13]);
			}
		if(playercount == 6){
		pawn[0] = new Pawn(SuspectName.Colonel_Mustard,Color.yellow, gb[11][10]);
		pawn[1] = new Pawn(SuspectName.Professor_Plum,Color.pink , gb[12][10]);
		pawn[2] = new Pawn(SuspectName.Rev_Green,Color.green , gb[11][11]);
		pawn[3] = new Pawn(SuspectName.Mrs_Peacock,Color.blue , gb[12][12]);
		pawn[4] = new Pawn(SuspectName.Miss_Scarlet,Color.red , gb[11][13]);
		pawn[5] = new Pawn(SuspectName.Mrs_White,Color.white ,  gb[12][13]);
		}

		playernumber = 0;
		Choosing = 1;
        CreateContent();
        AddEventListen();
        diceEnable = 1;
        moveflag = 0;
        showmoveable = 0;
        rise = 0;
        first = 0;
        hp = 1;
        hp2 = 1;
	}
 //and in this class I build the connection of buttons or panels in the user interface and seting the attribute for my playing UI
    public void CreateContent(){
        this.setSize(1200, 1000);
        this.setLocation( this.getToolkit().getScreenSize().width / 2- this.getWidth() / 2, 
                          this.getToolkit().getScreenSize().height/ 2 - this.getHeight() / 2);
        this.setLayout(new BorderLayout(20,20));
         Container contentPane = this.getContentPane();  
         contentPane.setBackground(Color.red); //   
         map = new JPanel();
         map.setBackground(Color.yellow); 
         map.setLayout(new BorderLayout());
         MGB = new MainGB();
         map.add(MGB);
         
         Font font = new Font("Courier", Font.BOLD,24);
         Font font2 = new Font("Courier", Font.BOLD,16);
         dice = new JButton("dice");
         dice.setPreferredSize(new Dimension(203,100));
         dice.setFont(font);
         dice.setBackground(Color.gray);
         dice.addActionListener(this);
         
         hypothesis = new JButton("hypothesis");
         hypothesis.setFont(font);
         hypothesis.setBackground(Color.gray);
         hypothesis.setPreferredSize(new Dimension(203,100));
         hypothesis.addActionListener(this);
         
         hypothesis_Polling = new JButton("Polling");
         hypothesis_Polling.setFont(font);
         hypothesis_Polling.setBackground(Color.gray);
         hypothesis_Polling.setPreferredSize(new Dimension(203,100));
         hypothesis_Polling.addActionListener(this);
         
         accusation = new JButton("accusation");
         accusation.setFont(font);
         accusation.setBackground(Color.gray);
         accusation.setPreferredSize(new Dimension(203,100));
         accusation.addActionListener(this);
         
         notbook = new JButton("notbook");
         notbook.setFont(font);
         notbook.setBackground(Color.gray);
         notbook.setPreferredSize(new Dimension(203,100));      
         notbook.addActionListener(this);
         
         pane2 = new JPanel(); 
         pane2.setLayout(new GridLayout(5, 1));
         pane2.add(dice);
         pane2.add(hypothesis); 
         pane2.add(hypothesis_Polling); 
         pane2.add(accusation);
         pane2.add(notbook);
         
         
         	
         lblCurEvent = new JTextField("");
         lblCurEvent.setPreferredSize(new Dimension(200,100));
         lblCurEvent.setBackground(Color.black);
         lblCurEvent.setForeground(Color.gray);
         lblCurEvent.setFont(font);
         lblCurEvent.setCaretColor(Color.black);
         lblCurEvent.setEditable(false);
         
         textarea = new JTextArea("---------------Recording Area---------------\n Player 1's turn");
         textarea.setBackground(Color.gray);    
         textarea.setPreferredSize(new Dimension(210,50));
         textarea.setCaretPosition(textarea.getText().length());
         JScrollPane jsp = new JScrollPane(textarea);
         jsp.setVerticalScrollBarPolicy( 
         JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED); 
         textarea.setEditable(false);
         
         positioninfor = new JTextField("");
         positioninfor.setBackground(Color.black);
         positioninfor.setPreferredSize(new Dimension(200,100));
         positioninfor.setForeground(Color.gray);
         positioninfor.setCaretColor(Color.black);
         positioninfor.setFont(font2);
         positioninfor.setEditable(false);

         this.setTitle("Main Game Board");
         contentPane.add(positioninfor,BorderLayout.SOUTH);
         contentPane.add(textarea,BorderLayout.WEST);
         contentPane.add(pane2,BorderLayout.EAST);
         contentPane.add(lblCurEvent, BorderLayout.NORTH); 
         contentPane.add(map, BorderLayout.CENTER);

         this.setVisible(true);
         this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    // Drawing the basic map and also draw map for avaliable movement and pawn, it also set the "inroom" area for slots.
	class MainGB extends JPanel{
		private static final long serialVersionUID = 1L;

		public void paint(Graphics g){
	        super.paint(g);
	        g.drawRect(3, 3,722, 722);	
	        for(int i = 0;i<24;i++){
		        for(int j = 0;j<24;j++){
			        g.drawRect(5+30*i, 5+30*j, 28, 28);		      

		        }
	        }
	        ((Graphics2D)g).setStroke(new BasicStroke(4.0f));
	        g.setColor(Color.black);
	        g.drawRect(4, 4,720, 720);	
	        g.drawRect(4, 4,180, 180);			//Kitchen
	        g.drawRect(244, 4,240, 210);		//Ballroom
	        g.drawRect(544, 4,180, 150);		//Conservatory
	        g.drawRect(4, 274 ,240, 180);		//Dining_Room
	        g.drawRect(544, 214,180, 150);		//Billiard_Room
	        g.drawRect(514, 394,210, 150);		//Library
	        g.drawRect(514, 604,210, 120);		//Study
	        g.drawRect(274, 514,180, 210);		//Hall
	        g.drawRect(4, 544,210, 180);		//Lounge
	        
	        g.setColor(Color.yellow);
	        g.drawRect(334, 304,60, 120);		//Beginning
	       
	        g.setColor(Color.white);
	        g.drawLine(124, 184, 154, 184);
	        g.drawLine(244, 184, 244, 154);
	        g.drawLine(484, 184, 484, 154);
	        g.drawLine(274, 214, 304, 214);
	        g.drawLine(454, 214, 424, 214);
	        g.drawLine(544, 154,544, 124);
	        g.drawLine(544, 154,574, 154);
	        g.drawLine(244, 304 ,244, 334);	
	        g.drawLine(214, 454 ,184, 454);	
	        g.drawLine(544, 244,544, 274);
	        g.drawLine(694, 364,664, 364);
	        g.drawLine(634, 394,604, 394);
	        g.drawLine(514, 454,514, 484);
	        g.drawLine(184, 544,214, 544);
	        g.drawLine(334, 514,394, 514);
	        g.drawLine(454, 604,454, 574);
	        g.drawLine(514, 604,544, 604);
	        ((Graphics2D)g).setStroke(new BasicStroke(12.0f));
	        Font font = new Font("Courier", Font.BOLD,32);
	        g.setFont(font);
	        g.setColor(Color.darkGray);
	        g.drawString("Corridor", 300 ,370);
	        g.drawString("Kitchen", 30 ,100);
	        g.drawString("Ballroom", 300 ,120);
	        g.drawString("Conservatory", 520 ,90);
	        g.drawString("Dining_Room", 20 ,370);
	        g.drawString("Library", 570 ,480);
	        g.drawString("Hall", 335 ,630);
	        g.drawString("Study", 580 ,660);
	        g.drawString("Billiard_Room", 510 ,300);
	        g.drawString("Lounge", 40 ,640);
	        if(showmoveable == 1){
	        	for(int i = 0;i<possible.size();i++){
        	 	g.setColor(Color.blue);
		        g.fillRect(5+30*possible.get(i).GetX(),5+30*possible.get(i).GetY(), 28, 28);
	        	}
	        }
	        for(int i=0;i<pawn.length;i++){
	        	 g.setColor(pawn[i].GetColor());
	 	        g.fillOval(pawn[i].GetSlot().GetX()*30+4, pawn[i].GetSlot().GetY()*30+4,30,30);	
	        }
	        
	    }
	}
    //Listener to mouse for reaction player's operation.
    public void AddEventListen(){

        map.addMouseMotionListener(this);
        map.addMouseListener(this);

    }
    
    
    public void SetEventToLable(String strEvent){
        lblCurEvent.setText(strEvent);
    }
    public void Setinroominfo(String inroominfo){
    	positioninfor.setText(inroominfo);
    }
    
    
    public void mouseDragged(MouseEvent e) {

    }
    // it will shown your mouse's position on top JTextField for higher accuracy.
    @Override
    public void mouseMoved(MouseEvent e) {
    	if(Choosing == 1){
    	strEvent = "Please choose your destantion. Your Mouse pointing : X=" + (int)((e.getX() - 4)/30+ 1) + " Y=" + (int)((e.getY() - 4)/30+ 1);
	        SetEventToLable(strEvent);
    	}
    }
    // cooperate with dice button for let player can move easier on playboard.
    @Override
    public void mouseClicked(MouseEvent e) {
        if(showmoveable == 1){
        	if( possible.contains(gb[(int)e.getX()/30][(int)e.getY()/30])){	
                Move(playernumber,gb[(int)e.getX()/30][(int)e.getY()/30]);
        		showmoveable = 0;
                strEvent = "Your pawn moved to X=" + (int)(e.getX()/30+ 1) + " Y=" + (int)(e.getY()/30+ 1)+"it is in room "+gb[(int)e.getX()/30][(int)e.getY()/30].InRoom();
                inroominfo = " ";
                for(int tem = 0;tem<playercount;tem++){
                	inroominfo += "PLAYER "+(tem+1)+": in "+pawn[tem].GetSlot().InRoom().name()+"     ";
                }
                Setinroominfo(inroominfo);
        	}
        	else{
        		strEvent = "It is not a avaliable position to move, please rechoose a position shown in blue area.";
            }
        }
        else{
        	strEvent = "Please choose your Operation first.";
        }
        SetEventToLable(strEvent);
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {   
    	if(Choosing == 1){
        	strEvent = "Please choose your Operation on right buttons.";
            SetEventToLable(strEvent);
        	}
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }
    //button's reaction methods
	@Override
	public void actionPerformed(ActionEvent e) {
		// active when people press dice button, the escond press will not success for obey the game rule.
		if(e.getSource()==dice && diceEnable == 1){
			diceresult1 = (int)(100*Math.random()%6+1);		
			diceresult2 = (int)(100*Math.random()%6+1);		
			strEvent ="result of dice are: " + diceresult1 + " and " + diceresult2;
			 SetEventToLable(strEvent);
			 diceEnable = 0;
			 possible = this.possible(playernumber, diceresult1 + diceresult2);
			 showmoveable = 1;
			 moveflag = 1;
			 map.repaint();
		}
		//When press button hypothesis
		if(e.getSource()==hypothesis){
			hw = new HypothesisWindow(pawn[playernumber].GetSlot().InRoom());
			hp = 1;
			hp2 = 1;
		}
		//When press button Polling, it will polling your hypothesis to each player and every player will collect useful informathion automaticly
		//such as three player claim that they do not have any card you mentioned, so all other players and you will record they do not have them in notebook
		// and I added two flag for protect this method will not be access when people in Corridor or seclect less seclection when they press last button
		if(e.getSource()==hypothesis_Polling){
			if(hw.Success()!=3){
				strEvent = "You did not chose enough selection, please press last button again." ;
		    	SetEventToLable(strEvent);
				 hp = 0;
			}
			if(hp == 1){
	    	Card[] Guess = hw.GuessResult();
			if( Guess[1].GetInfo().toString().equals(RoomName.Corridor.toString())){
				strEvent = "Seems you are in Corridor. Good luck next time!" ;
		    	SetEventToLable(strEvent);
				strEvent = "Good luck next time!" ;
	            textarea.append("\n "+strEvent);
	            hp2 = 0;
	            first = 1; 
	            rise = 1;
			}
			if(hp2 == 1){
	    	strEvent = "\nPlayer "+ (playernumber+1) +" hypothesised: \n"+Guess[0].GetInfo()+",in the "+Guess[1].GetInfo()+"\n with the "+Guess[2].GetInfo()+".\n" ;
	    	SetEventToLable(strEvent);
            textarea.append(" "+strEvent);
            int teem = playernumber+1;
            while(teem!=playernumber && rise == 0){
            	if(teem==playercount){
            		teem = 0;
            	}
            	if(players[teem].hypothesis(Guess[0].GetInfo(), Guess[1].GetInfo(), Guess[2].GetInfo())){
            		rise = 1;
            		Card[] show = players[teem].ShowCard();
            		int ttem = 0;
            		//here I set that the player without control will always show the first index card they have when been Hypothesised
            		while(ttem<show.length && first == 0){  
            			if(show[ttem].GetInfo().equals(Guess[0].GetInfo()) || show[ttem].GetInfo().equals(Guess[1].GetInfo()) || show[ttem].GetInfo().equals(Guess[2].GetInfo())){
            				first = 1;
            				strEvent = "Player "+ (teem+1) +" own "+show[ttem].GetInfo()+".\n" ;
            		    	SetEventToLable(strEvent);
            	            	players[playernumber].AddMark(show[ttem], teem);
            			}            			
            			ttem++;
            		}
            	}else{
            		
                	for(int teep = 0;teep<playercount;teep++){
                		if(teep!=teem){
                			players[teep].Nothave(Guess[0], teem);
                			players[teep].Nothave(Guess[1], teem);
                			players[teep].Nothave(Guess[2], teem);               			
                		}
                	}	
                }
            	teem++;            	
            	}
			}
			} 
		}
		if(e.getSource()==accusation){
			AccusationWindow aw = new AccusationWindow(truth, (playernumber+1));		
		}
		if(e.getSource()==notbook){
			ShowNoteBook snb = new ShowNoteBook(playercount, players[playernumber].ShowCard(), players[playernumber].ShowNote());
			if(first == 1 && rise == 1){
				playernumber++;
				if(playernumber==playercount)
					playernumber = 0;
				first = 0;
				rise = 0;
				diceEnable = 1;
				strEvent = "Please make sure to closed the notebook and then ask the next player to play." ;
		    	SetEventToLable(strEvent);
		    	strEvent = "Player " +(playernumber+1)+ "'s turn";
		    	SetEventToLable(strEvent);
	            textarea.append("\n "+strEvent);
			}
		}
	}
	// some method related to graph changing on the map
	public void Move(int pawn, Slot newposition) {
		if(moveflag == 1){
		this.pawn[pawn].Move(newposition);
		
		 map.repaint();
		 moveflag = 0;
		}
	}
	// Basing steps you can move to calculate the area you can reach
	public ArrayList<Slot> possible(int pawn, int steps) {					
		ArrayList<Slot> al =  new ArrayList<Slot>();
		al.add(this.pawn[pawn].GetSlot());
		for(int i = 1;i<= steps; i++){
			int size = al.size();
			for(int j = 0;j<size; j++){
				Slot target = al.get(j);
				if(!target.DownIsWall())
				if(!al.contains(this.gb[target.GetX()][target.GetY()+1]) && !this.pawn[0].GetSlot().equals(this.gb[target.GetX()][target.GetY()+1])){
					al.add(this.gb[target.GetX()][target.GetY()+1]);
				}
				if(!target.UpIsWall())
				if(!al.contains(this.gb[target.GetX()][target.GetY()-1]) && !this.pawn[0].GetSlot().equals(this.gb[target.GetX()][target.GetY()-1])){
					al.add(this.gb[target.GetX()][target.GetY()-1]);
				}
				if(!target.LeftIsWall())
				if(!al.contains(this.gb[target.GetX()-1][target.GetY()]) && !this.pawn[0].GetSlot().equals(this.gb[target.GetX()-1][target.GetY()])){
					al.add(this.gb[target.GetX()-1][target.GetY()]);
				}
				if(!target.RightIsWall())
				if(!al.contains(this.gb[target.GetX()+1][target.GetY()]) && !this.pawn[0].GetSlot().equals(this.gb[target.GetX()+1][target.GetY()])){
					al.add(this.gb[target.GetX()+1][target.GetY()]);
				}
				if(!al.contains(this.gb[23][23]) && target.GetX()==0 && target.GetY()==0){
					al.add(this.gb[23][23]);
				}
				if(!al.contains(this.gb[0][0]) && target.GetX()==23 && target.GetY()==23){
					al.add(this.gb[0][0]);
				}
				if(!al.contains(this.gb[0][23]) && target.GetX()==23 && target.GetY()==0){
					al.add(this.gb[0][23]);
				}
				if(!al.contains(this.gb[23][0]) && target.GetX()==0 && target.GetY()==23){
					al.add(this.gb[23][0]);
				}
				for(int p = 0;p < this.pawn.length;p++){
					if(al.contains(this.pawn[p].GetSlot())){
						al.remove(this.pawn[p].GetSlot());
					}
				}
			}
		}
		return al;
	}
}
