package ucd.ie.cluedo;
//class for Player's attribute and enclude some method for test hypothesis or adding information to notebook( because notebook class is belongs to this class)
public class Player {
	private String nickname;
	private Card[] cards;
	private NoteBook note;
	private int cardcount;
	
	public Player(int playercount){
		cardcount = 0;
		cards = new Card[1];
		note = new NoteBook(playercount);
	}
	public void SetName(String nickname){
		this.nickname = nickname;
	}
	public String GetName(){
		return nickname;
	}
	//these two method are connect with notebook's add and minus
	public void AddMark (Card card, int PlayerNumber){
		note.add(card, PlayerNumber);
	}
	public void Nothave (Card card, int PlayerNumber){
		note.minus(card, PlayerNumber);
	}
	public int[][] ShowNote(){
		return note.Show();
	}
	public void PickCard (Card card){
		Card[] nc = new Card[cardcount+1];
		for(int i = 0;i<cardcount;i++){
			nc[i] = cards[i];
		}
		 nc[cardcount] = card;
		 note.owned(card);
		 cards = nc;
		 cardcount++;
	}
	public Card[] ShowCard(){
		return cards;
	} 
	// this method will return aboolean value for answer a Hypothesis
	public Boolean hypothesis (String suspect, String room, String weapon){
		for(int temp = 0;temp<cardcount;temp++){
			if(cards[temp].GetInfo().equals(suspect)||cards[temp].GetInfo().equals(room)||cards[temp].GetInfo().equals(weapon)){
				return true;
			}
		}
		return false;
	}
	

}
