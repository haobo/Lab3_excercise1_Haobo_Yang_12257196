package ucd.ie.cluedo;

import ucd.ie.cluedo.CardType.CardsType;
import ucd.ie.cluedo.CardType.RoomName;

//class for Room's attribute
public class RoomCard implements Card{
	RoomName Room;
	CardsType type = CardsType.Room;
	Player owner;
	public RoomCard(RoomName Room){
		this.Room = Room;
	}
	public String GetInfo() {
		return Room.toString();
	}
	public RoomName GetName() {
		return Room;
	}
	public CardsType GetType() {
		return type;
	}

	public void SetOwner(Player owner) {
		this.owner = owner;
	}

	public Player GetOwner() {
		return owner;
	}


}
