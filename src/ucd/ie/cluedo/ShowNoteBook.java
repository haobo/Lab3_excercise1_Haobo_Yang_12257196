package ucd.ie.cluedo;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JTextArea;

import ucd.ie.cluedo.CardType.RoomName;
import ucd.ie.cluedo.CardType.SuspectName;
import ucd.ie.cluedo.CardType.WeaponName;
// this class extends JFrame for showing notebook to each player
public class ShowNoteBook extends JFrame {
	private Card[] b = {new SuspectCard(SuspectName.Colonel_Mustard), new SuspectCard(SuspectName.Professor_Plum), new SuspectCard(SuspectName.Rev_Green),
			   new SuspectCard(SuspectName.Mrs_Peacock), new SuspectCard(SuspectName.Miss_Scarlet),new SuspectCard(SuspectName.Mrs_White),
			   new RoomCard(RoomName.Kitchen),  new RoomCard(RoomName.Ballroom),  new RoomCard(RoomName.Conservatory),
			   new RoomCard(RoomName.Dining_Room),  new RoomCard(RoomName.Library),  new RoomCard(RoomName.Hall),  new RoomCard(RoomName.Study),
			   new RoomCard(RoomName.Billiard_Room),  new RoomCard(RoomName.Lounge), new WeaponCard(WeaponName.Candlestick), new WeaponCard(WeaponName.Knife), 
			   new WeaponCard(WeaponName.lead_Pipe) , new WeaponCard(WeaponName.Revolver) , new WeaponCard(WeaponName.Rope) ,
			   new WeaponCard(WeaponName.Poison) };
	public ShowNoteBook(int playercount,Card[] own,int[][] nb){
		this.setSize(500, 900);
    	this.setLocation( this.getToolkit().getScreenSize().width / 2- this.getWidth() / 2, 
                this.getToolkit().getScreenSize().height/ 2 - this.getHeight() / 2);
    	this.setLayout(new BorderLayout(20,20));
    	Font font1 = new Font("Courier", Font.BOLD,24);
    	Font font2 = new Font("Courier", Font.BOLD,12);    	
    	JTextArea p1 = new JTextArea("                      my Notebook");
    	p1.setFont(font1);
    	p1.setEditable(false);   	
    	JTextArea p = new JTextArea("");
    	JTextArea p2 = new JTextArea("");
    	p.setPreferredSize(new Dimension(200,100));
    	p.setBackground(Color.gray);
    	p.setForeground(Color.white);
    	p.setCaretColor(Color.gray);
    	p.setEditable(false); 	
    	p.setFont(font2); 
    	
    	for(int te = 1;te<=playercount;te++){
        	p.append("    Player "+te);
    	}
		p.append("\n\n");
    	for(int t = 0;t<21;t++){
			for(int j = 0;j<playercount;j++){
				if(j == 0)
					p.append("          " + nb[t][j]);	
				else
					p.append("                 " + nb[t][j]);	
			}
			p.append("\n\n");	
		}
    	p2.setBackground(Color.gray);
    	p2.setForeground(Color.white);
    	p2.setCaretColor(Color.gray);
    	p2.setEditable(false); 	
    	p2.setFont(font2); 
    	p2.append("\n\n");
    	for(int t = 0;t<21;t++){
    		p2.append(b[t].GetInfo()+ "  ");	
			p2.append("\n\n");	
		}
    	this.add(p1,BorderLayout.NORTH);
        this.add(p,BorderLayout.CENTER);  
        this.add(p2,BorderLayout.WEST);  
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);   // this setting make sure it will not affect my main play process when this class is closed by user
        this.setVisible(true);  
        
       
	}

}
