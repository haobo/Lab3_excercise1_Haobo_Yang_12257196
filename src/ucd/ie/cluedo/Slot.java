package ucd.ie.cluedo;

import ucd.ie.cluedo.CardType.RoomName;
//A slot recording one block's property and position
public class Slot {
	private	int x;
	private int y;
	private RoomName inroom;
	private boolean avalible;
	private boolean up;
	private boolean down;
	private boolean left;
	private boolean right;
	
	public Slot(int x, int y){
		this.x = x;
		this.y = y;
		inroom = RoomName.Corridor;
		up = false;
		down = false;
		left = false;
		right = false;	
	}
	public int GetX(){
		return x;
	}
	public int GetY(){
		return y;
	}
	public void SetInRoom(RoomName inroom){
		this.inroom = inroom;
	}
	public boolean UpIsWall(){
		return up;
	}	
	public boolean DownIsWall(){
		return down;
	}
	public boolean LeftIsWall(){
		return left;
	}
	public boolean RightIsWall(){
		return right;
	}
	public void SetUpIsWall(){
		up = true;
	}	
	public void SetDownIsWall(){
		down = true;
	}
	public void SetLeftIsWall(){
		left = true;
	}
	public void SetRightIsWall(){
		right = true;
	}
	public RoomName InRoom(){
		return inroom;
	}
	public boolean IsAvalible(){
		return avalible;
	}
}
