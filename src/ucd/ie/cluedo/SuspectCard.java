package ucd.ie.cluedo;

import ucd.ie.cluedo.CardType.CardsType;
import ucd.ie.cluedo.CardType.SuspectName;

//class for Suspect's attribute
public class SuspectCard implements Card{
	SuspectName name;
	CardsType type = CardsType.Suspect;
	Player owner;
	public SuspectCard(SuspectName name){
		this.name = name;
	}
	public String GetInfo() {
		return name.toString();
	}

	public CardsType GetType() {
		return type;
	}

	public void SetOwner(Player owner) {
		this.owner = owner;
	}

	public Player GetOwner() {
		return owner;
	}

}
