package ucd.ie.cluedo;

import ucd.ie.cluedo.CardType.CardsType;
import ucd.ie.cluedo.CardType.WeaponName;
// class for weapon's attribute
public class WeaponCard implements Card{
	WeaponName Weapon;
	CardsType type = CardsType.Weapon;
	Player owner;
	public WeaponCard(WeaponName Weapon){
		this.Weapon = Weapon;
	}
	public String GetInfo() {
		return Weapon.toString();
	}

	public CardsType GetType() {
		return type;
	}

	public void SetOwner(Player owner) {
		this.owner = owner;
	}

	public Player GetOwner() {
		return owner;
	}
}
