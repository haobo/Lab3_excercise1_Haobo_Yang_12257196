package ucd.ie.cluedo;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JTextField;
// this class extends JfFrame for show the result, win or lose.
public class Win extends JFrame {
	private JTextField tfield;
	public Win(boolean win, int player){
		this.setTitle("Conclusion");
	this.setSize(500, 400);
	this.setLocation( this.getToolkit().getScreenSize().width / 2- this.getWidth() / 2, 
            this.getToolkit().getScreenSize().height/ 2 - this.getHeight() / 2);
	Font font = new Font("Courier", Font.BOLD,36);
	tfield = new JTextField("");
	tfield.setPreferredSize(new Dimension(200,100));

	if(win){
		tfield.setBackground(Color.yellow);
		tfield.setForeground(Color.red);
		tfield.setFont(font);
		tfield.setEditable(false);
		tfield.setText("       Winner is Player "+player+"!");
	}else{
		tfield.setBackground(Color.gray);
		tfield.setForeground(Color.magenta);
		tfield.setFont(font);
		tfield.setEditable(false);
		tfield.setText("        You lose...............");
	}
	this.getContentPane().add(tfield);
    this.setVisible(true);
    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
}
